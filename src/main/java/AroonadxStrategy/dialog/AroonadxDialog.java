package AroonadxStrategy.dialog;

import AroonadxStrategy.AroonadxSummary;
import build.ui.AlgoDialog;
import build.utils.ArqLogger;
import build.utils.StandardTimes;
import enums.PORTFOLIO_TYPE;
import javafx.scene.control.Alert;

import java.time.LocalTime;
import java.util.logging.Level;

public class AroonadxDialog extends AlgoDialog<AroonadxSummary> {

    private boolean m_editMode;
    private AroonadxPane m_pane;

    public AroonadxDialog(Boolean b) {
        m_editMode = !b;
        m_pane = new AroonadxPane(m_editMode);
        setAlgoDialogPane(m_pane);
    }

    @Override
    public void loadParams(AroonadxSummary aroonadxSummary) {
        m_pane.setSymbol(aroonadxSummary.getSymbol());
        m_pane.setStartTime(aroonadxSummary.startTimeProperty().get());
        m_pane.setEndTime(aroonadxSummary.endTimeProperty().get());
         m_pane.setEmaPeriod1(aroonadxSummary.getEmaPeriod1());
        m_pane.setEmaPeriod2(aroonadxSummary.getEmaPeriod2());
        m_pane.setEmaPeriod3(aroonadxSummary.getEmaPeriod3());
        m_pane.setAdxPeriod(aroonadxSummary.getAdxPeriod());
        m_pane.setAroonPeriod(aroonadxSummary.getAroonPeriod());
        m_pane.setInterval(aroonadxSummary.getInterval());
//        m_pane.setClientName(aroonadxSummary.getClientName());
        m_pane.setMargin(aroonadxSummary.getMargin());


    }

    @Override
    public AroonadxSummary readParams() {
        AroonadxSummary aroonadxSummary = m_editMode ? getSelectedRow() : new AroonadxSummary();
        if (!m_editMode)
        {
            aroonadxSummary.setSymbol(m_pane.getSymbol());
            aroonadxSummary.setStrategy("AROONADX");
            aroonadxSummary.setBroker("XTS");
            aroonadxSummary.setPortfolioType(PORTFOLIO_TYPE.PAPER_TRADING);
        }
        aroonadxSummary.setSymbol(m_pane.getSymbol());
        aroonadxSummary.setStartTime(m_pane.getStartTime());
        aroonadxSummary.setEndTime(m_pane.getEndTime());
        aroonadxSummary.setEmaPeriod1(m_pane.getEmaPeriod1());
        aroonadxSummary.setEmaPeriod2(m_pane.getEmaPeriod2());
        aroonadxSummary.setEmaPeriod3(m_pane.getEmaPeriod3());
        aroonadxSummary.setAdxPeriod( m_pane.getAdxPeriod());
        aroonadxSummary.setAroonPeriod( m_pane.getAroonPeriod());
        aroonadxSummary.setInterval(m_pane.getInterval());
        aroonadxSummary.setMargin(m_pane.getMargin());
//        aroonadxSummary.setClientName(m_pane.getClientName());
        return aroonadxSummary;
    }

    @Override
    public boolean validateParams() {

        String startTime = m_pane.getStartTime();
        if(startTime == null || startTime.isEmpty())
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Start time:"+startTime+" is invalid");
            alert.setContentText("Please input valid Start time");
            alert.showAndWait();
            return false;
        }

        LocalTime st;
        try {
            st = LocalTime.parse(
                    startTime,
                    StandardTimes.FORMAT);
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Start time:" + startTime + " format is invalid");
            alert.setContentText("Please input valid Start time in format HH:mm:ss ");
            alert.showAndWait();
            return false;
        }

        String endTime = m_pane.getEndTime();
        LocalTime et;
        try {
            et = LocalTime.parse(
                    endTime,
                    StandardTimes.FORMAT);
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("End time:" + endTime + " format is invalid");
            alert.setContentText("Please input valid End time in format HH:mm:ss ");
            alert.showAndWait();
            return false;
        }


        String tradeSymbol = m_pane.getSymbol();
        if (tradeSymbol == null || tradeSymbol.isEmpty())
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Trade symbol is invalid");
            alert.setContentText("Please input valid trade symbol");
            alert.showAndWait();
            return false;
        }

        if (st.compareTo(et) != -1) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Time Check error");
            alert.setContentText("Start time should be less than end time");
            alert.showAndWait();
            return false;
        }
        if (st.compareTo(LocalTime.now()) != 1) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Time Check error");
            alert.setContentText("Start time should be greater than current time");
            alert.showAndWait();
            return false;
        }

        return true;
    }
}
