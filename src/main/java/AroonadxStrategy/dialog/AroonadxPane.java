package AroonadxStrategy.dialog;
import build.common.Instrument;
import build.ui.*;
import build.utils.StandardTimes;
import enums.EXCHANGE;
import enums.INSTRUMENT_TYPE;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AroonadxPane extends AlgoDialogPane
{
    private TimeSpinner startTime;
    private TimeSpinner endTime;
    private ComboBox <String>symbol;
    private EditableSpinner emaPeriod1;
    private EditableSpinner emaPeriod2;
    private EditableSpinner emaPeriod3;
    private EditableSpinner adxPeriod;
    private EditableSpinner aroonPeriod;
    private EditableSpinner interval;
    private EditableSpinner margin;
//    private FilterComboBox clientName;

    public int getInterval() {
        return (int) interval.getValue();
    }

    public void setInterval(int interval) {
        this.interval.getValueFactory().setValue(interval);
    }

    public String getStartTime() {
        return (startTime.getValue()).format(StandardTimes.FORMAT);
    }
    public void setStartTime(String startTime) {
        this.startTime.getValueFactory().setValue(LocalTime.parse(startTime, StandardTimes.FORMAT));
    }

    public int getMargin() {
        return (int) margin.getValue();
    }

    public void setMargin(int margin) {
        this.margin.getValueFactory().setValue(margin);
    }

    public String getEndTime() {
        return (endTime.getValue()).format(StandardTimes.FORMAT);//return endTime.getTimeStr();
    }
    public void setEndTime(String endTime) {
        this.endTime.getValueFactory().setValue(LocalTime.parse(endTime, StandardTimes.FORMAT));//this.endTime.setTimeStr(endTime);
    }
    public String getSymbol() {
        return symbol.getValue();
    }
    public void setSymbol(String symbol) {
        this.symbol.setValue(symbol);
    }
//    public String getClientName() {
//        return (String)clientName.get().getValue();
//    }
//    public void setClientName(String clientName) {
//        this.clientName.get().setValue(clientName);
//    }
    public int getEmaPeriod1() {
        return (int) emaPeriod1.getValue();
    }
    public void setEmaPeriod1(int emaPeriod) {
        this.emaPeriod1.getValueFactory().setValue(emaPeriod);
    }
    public int getEmaPeriod2() {
        return (int) emaPeriod2.getValue();
    }
    public void setEmaPeriod2(int emaPeriod) {
        this.emaPeriod2.getValueFactory().setValue(emaPeriod);
    }
    public int getEmaPeriod3() {
        return (int) emaPeriod3.getValue();
    }
    public void setEmaPeriod3(int emaPeriod) {
        this.emaPeriod3.getValueFactory().setValue(emaPeriod);
    }

    public int getAdxPeriod() {
        return (int)adxPeriod.getValue();
    }
    public void setAdxPeriod(int adxPeriod) {
        this.adxPeriod.getValueFactory().setValue(adxPeriod);
    }

    public int getAroonPeriod() {
        return (int)aroonPeriod.getValue();
    }
    public void setAroonPeriod(int aroonPeriod) {
        this.aroonPeriod.getValueFactory().setValue(aroonPeriod);
    }

    public AroonadxPane(boolean nonEditMode)
    {
        init();
        
        if(nonEditMode)
        {
            EditNotAllowed();
        }
    }
    
    private void EditNotAllowed()
    {
        symbol.setDisable(true);
    }
    
    private void init()
    {
        startTime = new TimeSpinner(LocalTime.parse("09:15:00", StandardTimes.FORMAT), //DateTimeFormatter.ofPattern("H:mm:ss")
                LocalTime.parse("23:30:00", StandardTimes.FORMAT),
                LocalTime.parse("09:15:00", StandardTimes.FORMAT),
                StandardTimes.FORMAT);


        endTime = new TimeSpinner(LocalTime.parse("09:15:00", StandardTimes.FORMAT),
                LocalTime.parse("23:30:00", StandardTimes.FORMAT),
                LocalTime.parse("15:30:00", StandardTimes.FORMAT),
                StandardTimes.FORMAT);

        emaPeriod1 = new EditableSpinner(1, 1000000, 5, 1);
        emaPeriod2 = new EditableSpinner(1, 1000000, 15, 1);
        emaPeriod3 = new EditableSpinner(1, 1000000, 25, 1);
        adxPeriod = new EditableSpinner(1, 1000000, 14, 1);
        aroonPeriod = new EditableSpinner(1, 1000000, 14, 1);
        List<String> symbols = getAllInstruments(i->i.getExchange().equals(EXCHANGE.NSE) &&
                i.getType().equals(INSTRUMENT_TYPE.EQUITY))
                .stream()
                .map(Instrument::getSymbol)
                .collect(Collectors.toList());
        symbol = new FilterComboBox<>(symbols).get();
        symbol.setValue("RELIANCE-EQ");
//        clientName= new FilterComboBox<>(new ArrayList<>());
        margin = new EditableSpinner(1, 1000000, 100000, 1000);
        interval=new EditableSpinner(1, 1000000, 1, 1);

        DecoratedHBox indicatorBox = new DecoratedHBox(true);
        indicatorBox.add(new Label("AROON Period:"));
        indicatorBox.addSpacer();
        indicatorBox.add(aroonPeriod);
        indicatorBox.addSpacer();
        indicatorBox.add(new Label("ADX Period:"));
        indicatorBox.addSpacer();
        indicatorBox.add(adxPeriod);
        indicatorBox.addSpacer();
        indicatorBox.add(new Label("Interval:    "));
        indicatorBox.addSpacer();
        indicatorBox.add(interval);

        DecoratedHBox clientmarginBox = new DecoratedHBox(true);
        clientmarginBox.add(new Label("Margin:               "));
        clientmarginBox.addSpacer();
        clientmarginBox.add(margin);
        clientmarginBox.addSpacer();
        clientmarginBox.add(new Label("Client Name:"));
        clientmarginBox.addSpacer();
//        clientmarginBox.add(clientName.get());

        addWidget(getTimeBox(), 0, 0, 9, 1);
        addWidget(getIndicatorsBox(), 0, 1, 9, 1);
        addWidget(indicatorBox,0,2,9,1);
        addWidget(clientmarginBox,0,3,6,1);
    }
    private Node getTimeBox(){
        DecoratedHBox Box = new DecoratedHBox(true);
        Box.add(new Label("Start Time:                  "));
        Box.addSpacer();
        Box.add(startTime);
        Box.addSpacer();
        Box.add(new Label("                            End Time:            "));
        Box.addSpacer();
        Box.add(endTime);
        Box.addSpacer();
        Box.add(new Label("       Symbol:              "));
        Box.addSpacer();
        Box.add(symbol);
         return Box;
    }

    private Node getIndicatorsBox() {
        DecoratedHBox indicatorBox = new DecoratedHBox(true);
        indicatorBox.add(new Label("EMA Period-1:    "));
        indicatorBox.addSpacer();
        indicatorBox.add(emaPeriod1);
        indicatorBox.addSpacer();
        indicatorBox.add(new Label("   EMA Period-2:    "));
        indicatorBox.addSpacer();
        indicatorBox.add(emaPeriod2);
        indicatorBox.addSpacer();
        indicatorBox.add(new Label("   EMA Period-3:"));
        indicatorBox.addSpacer();
        indicatorBox.add(emaPeriod3);
        return indicatorBox;
    }


    @Override
    public String getHeaderText() {
        return "AROONADX Strategy";
    }

    @Override
    public int getTotalGridColumns() {
        return 7;
    }

    @Override
    public int getTotalGridRows() {
        return 1;
    }

    @Override
    public double getGridHGap() {
        return 3;
    }

    @Override
    public double getGridVGap() {
        return 3;
    }

    @Override
    public int getGridRowHeight() {
        return (DEFAULT_GRID_ROW_HEIGHT+1);
    }
}
