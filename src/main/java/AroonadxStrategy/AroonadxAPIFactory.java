package AroonadxStrategy;

import AroonadxStrategy.dialog.AroonadxDialog;
import AroonadxStrategy.strategy.AroonadxStrategy;
import build.strategy.APIResourceFactory;
import build.ui.ActionTableColumn;
import enums.STRATEGY_STATUS;
import javafx.scene.paint.Color;
import javafx.util.Pair;


public class AroonadxAPIFactory extends APIResourceFactory {
    public AroonadxAPIFactory(String s) {
        super(s);
    }

    @Override
    public void InitializeFactory() {
        this.setAlgoMonitorRowClass(AroonadxSummary.class);
        this.setDialogCreator(AroonadxDialog::new);
        this.setStrategyCreator(AroonadxStrategy::new);
        addAlgoTableColumn(ActionTableColumn.column("Portfolio", AroonadxSummary::portfolioProperty));
        ActionTableColumn<AroonadxSummary, STRATEGY_STATUS> statusColumn
                = ActionTableColumn.column("Status", AroonadxSummary::statusProperty);
        statusColumn.CELL_COLOR_GENERATOR = status -> switch (status) {
            case INVALID -> new Pair<>(Color.WHITE, Color.WHITESMOKE);
            case RECOVERED -> new Pair<>(Color.LIGHTBLUE, Color.DARKBLUE);
            case INITIALIZED -> new Pair<>(Color.WHITE, Color.LIGHTBLUE);
            case MODIFIED -> new Pair<>(Color.LIGHTBLUE, Color.GREEN);
            case DELETED -> new Pair<>(Color.LIGHTPINK, Color.RED);
            case RUNNING -> new Pair<>(Color.LIGHTGREEN, Color.DARKGREEN);
            case PAUSED -> new Pair<>(Color.LIGHTYELLOW, Color.BROWN);
            case STOPPED -> new Pair<>(Color.LIGHTGREY, Color.BLACK);
        };

        addAlgoTableColumn(statusColumn);
        addAlgoTableColumn(ActionTableColumn.column("Symbol", AroonadxSummary::symbolProperty));
        addAlgoTableColumn(ActionTableColumn.column("Symbol LTP", AroonadxSummary::symbolLtpProperty));
        addAlgoTableColumn(ActionTableColumn.column("Start Time", AroonadxSummary::startTimeProperty));
        addAlgoTableColumn(ActionTableColumn.column("End Time", AroonadxSummary::endTimeProperty));
        addAlgoTableColumn(ActionTableColumn.column("CreatedTime", AroonadxSummary::createdTimeProperty));
        addAlgoTableColumn(ActionTableColumn.column("EMA Period-1", AroonadxSummary::emaPeriod1Property));
        addAlgoTableColumn(ActionTableColumn.column("EMA Period-2", AroonadxSummary::emaPeriod2Property));
        addAlgoTableColumn(ActionTableColumn.column("EMA Period-3", AroonadxSummary::emaPeriod3Property));
        addAlgoTableColumn(ActionTableColumn.column("ADX Period", AroonadxSummary::adxPeriodProperty));
        addAlgoTableColumn(ActionTableColumn.column("AROON Period", AroonadxSummary::aroonPeriodProperty));
        addAlgoTableColumn(ActionTableColumn.column("Interval", AroonadxSummary::intervalProperty));
        addAlgoTableColumn(ActionTableColumn.column("Margin", AroonadxSummary::marginProperty));
        addAlgoTableColumn(ActionTableColumn.column("Client Name", AroonadxSummary::clientNameProperty));
    }

    @Override
    public String getTabName() {
        return "AROONADX";
    }
}
