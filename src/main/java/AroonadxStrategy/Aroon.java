package AroonadxStrategy;

import build.common.Candle;
import build.common.CandleProperty;
import build.technicalindicator.TechnicalIndicatorHelper;
import java.util.*;

public class Aroon implements TechnicalIndicatorHelper {
    int range;
    List<Candle> candles;
    public Aroon(int period, List<Candle> candleList)
    {
        this.candles = candleList;
        if (period < 1) {
            throw new IllegalArgumentException(this.getClass().getSimpleName() + ":period cannot be less than 1");
        } else {
            this.range = period;
        }
    }

    @Override
    public boolean initialized() {
        return false;
    }

    @Override
    public void initialize(Iterator<Candle> iterator) {
        Objects.requireNonNull(iterator);
        iterator.forEachRemaining(this::update);
    }

    @Override
    public void update(Candle candle) {

    }

    @Override
    public void update(double v) {

    }

    @Override
    public double get() {
        return 0;
    }

    @Override
    public void reset() {

    }

    @Override
    public void setCandleProperty(CandleProperty candleProperty) {

    }
    public List<Double> calculateAroonUp() {
            List<Double>aroonUp=new ArrayList<>();
            for (int i = range - 1; i < candles.size(); i++) {
            HighestHigh highestHigh = new HighestHigh();
            highestHigh.find(candles, i - range + 1, range);
            aroonUp.add(this.calcAroon((i - highestHigh.getIndex())));
        }
       // ArqLogger.getInstance().getLogger().log(Level.INFO,"Aroon Up Size: " + aroonUp.size());
        return aroonUp;
    }

    public List<Double> calculateAroonDown() {
            List<Double>aroonDown=new ArrayList<>();
            for (int i = range - 1; i < candles.size(); i++) {
            LowestLow lowestLow = new LowestLow();
            lowestLow.find(candles, i - range + 1, range);
            aroonDown.add( this.calcAroon((i - lowestLow.getIndex())));
            }
        //ArqLogger.getInstance().getLogger().log(Level.INFO,"Aroon Down Size: " + aroonDown.size());
        return aroonDown;
    }
    private double calcAroon(int marker) {
        System.out.println("Index: " + marker);
        System.out.println("Range: " + range);
        double finalValue = Math.round(100*(100.0 * ((range - marker)/(double)range)))/100.0;
        System.out.println("Final Value: " + finalValue);
        return finalValue;
    }
}