package AroonadxStrategy;
import build.common.Candle;
import java.util.List;

public class LowestLow {
    private int index;
    private double value;

    public void find(List<Candle> candle, int startIndex, int endIndex) {

        this.value = candle.get(startIndex).getLow();
        this.index = startIndex;
        System.out.println("Lowest high:"+ this.value);
        for (int i=startIndex; i < endIndex+startIndex; i++) {
            System.out.println("HIGH: "+candle.get(i).getLow());
            if (candle.get(i).getLow() < this.value) {

                this.value = candle.get(i).getLow();
                this.index = i;

            }
        }
    }

    public double getValue() {
        return this.value;
    }

    public int getIndex() {
        return this.index;
    }
}