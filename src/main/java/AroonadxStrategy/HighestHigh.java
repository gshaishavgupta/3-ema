package AroonadxStrategy;

import build.common.Candle;

import java.util.List;

public class HighestHigh {

    private int index;

    private double value;


    public void find(List<Candle> candles, int startIndex, int endIndex) {

        this.value = candles.get(startIndex).getHigh();
        this.index = startIndex;
        System.out.println("Highest high:"+ this.value);
        for (int i=startIndex; i < endIndex+startIndex; i++) {
           System.out.println("HIGH: "+candles.get(i).getHigh());
            if (candles.get(i).getHigh() > this.value) {

                this.value = candles.get(i).getHigh();
                this.index = i;

            }
        }
    }

    public double getValue() {
        return this.value;
    }

    public int getIndex() {
        return this.index;
    }
}