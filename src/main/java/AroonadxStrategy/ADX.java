package AroonadxStrategy;

import build.common.Candle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ADX {

    private final List<Candle> candleList;
    private int period;

    private final List<Double> tr;
    private final List<Double> trPeriod;
    //    private double[] posDM1;
//    private double[] negDM1;
    private final List<Double> posDM1;
    private final List<Double> negDM1;
    private final List<Double> posDmPeriod;
    private final List<Double> negDmPeriod;
    private final List<Double> posDiPeriod;
    private final List<Double> negDiPeriod;
    private final List<Double> diDiffPeriod;
    private final List<Double> diSumPeriod;

    private final List<Double> dx;
    private final List<Double> adx;

    public ADX(List<Candle> candleList) {
        this.candleList = candleList;
        tr = new ArrayList<>();
        trPeriod = new ArrayList<>();
        posDmPeriod = new ArrayList<>();
        negDmPeriod = new ArrayList<>();
        posDiPeriod = new ArrayList<>();
        posDM1 = new ArrayList<>();
        negDM1 = new ArrayList<>();
        negDiPeriod = new ArrayList<>();
        diDiffPeriod = new ArrayList<>();
        diSumPeriod = new ArrayList<>();
        dx = new ArrayList<>();
        adx = new ArrayList<>();
    }

    public void calculate(int period) {
        this.period = period;
        posDM1.clear();
        negDM1.clear();
        for (int i = 0; i < candleList.size(); i++) {

            if (i == 0)
                tr.add(0.0);
            if (i > 0) {
                // Calc True Range
                tr.add(i, new TrueRange().calculate(this.candleList.get(i).getHigh(), this.candleList.get(i).getLow(),
                        this.candleList.get(i - 1).getClose()).getTrueRange());

                // Calc +DM1
                posDM1.add(this.positiveDirectionalMovement(this.candleList.get(i).getHigh(), this.candleList.get(i - 1).getHigh(),
                        this.candleList.get(i).getLow(), this.candleList.get(i - 1).getLow()));

                // Calc -DM1
                negDM1.add(this.negativeDirectionalMovement(this.candleList.get(i).getHigh(), this.candleList.get(i - 1).getHigh(),
                        this.candleList.get(i).getLow(), this.candleList.get(i - 1).getLow()));
            }

            this.trueRangePeriod(i);

            this.positiveDirectionalMovementPeriod(i);
            this.negativeDirectionalMovementPeriod(i);
            this.positiveDirectionalIndicator(i);
            this.negativeDirectionalIndicator(i);

            this.directionalDiffPeriod(i);
            this.directionalSumPeriod(i);

            this.directionalIndex(i);
            this.averageDirectionalIndex(i);

        }

    }

    public List<Double> getADX() {
        return this.adx;
    }

    public List<Double> getPositiveDirectionalIndicator() {
        return this.posDiPeriod;
    }

    public List<Double> getNegativeDirectionalIndicator() {
        return this.negDiPeriod;
    }


    private double positiveDirectionalMovement(double high, double previousHigh, double low, double previousLow) {
        if ((high - previousHigh) > (previousLow - low)) {
            return Math.max((high - previousHigh), 0);
        } else {
            return 0;
        }
    }

    private double negativeDirectionalMovement(double high, double previousHigh, double low, double previousLow) {
        if ((previousLow - low) > (high - previousHigh)) {
            return Math.max((previousLow - low), 0);
        } else {
            return 0;
        }
    }

    private void trueRangePeriod(int idx) {

        if (idx == period) {

            // at Period, ie: typical ADX 14 right at 14 get determine initial
            // TR value
            double sum = 0;

            for (int i = 0; i < this.period; i++) {
                sum = sum + this.tr.get(idx - i);
            }

            this.trPeriod.add(idx, sum);

        } else if (idx > period) {

            // Use previous TR Period to build on current TR Period
            double prevTrPeriod = this.trPeriod.get(idx - 1);
            this.trPeriod.add(idx, prevTrPeriod - (prevTrPeriod / this.period) + this.tr.get(idx));

        } else {
            // Before Period, no calc required set to 0
            this.trPeriod.add(idx, 0.00);
        }

    }

    private void positiveDirectionalMovementPeriod(int idx) {

        if (idx == this.period) {
            // Determine First value of the given period
//            this.posDmPeriod.add(idx, Arrays.stream(Arrays.copyOfRange(this.posDM1.toArray(), 0, this.period + 1)).sum());
            this.posDmPeriod.add(idx, this.posDM1.stream().filter(i -> i > 0 && i < this.period + 1).mapToDouble(i -> i).sum());

        } else if (idx > this.period) {
            // Determine remaining values beyond the first value
            double prevPosDmPeriod = this.posDmPeriod.get(idx - 1);
            this.posDmPeriod.add(idx, prevPosDmPeriod - (prevPosDmPeriod / this.period) + this.posDM1.get(idx - 1));

        } else {
            // values for less then the given period
            posDmPeriod.add(idx, 0.00);
        }

    }

    private void negativeDirectionalMovementPeriod(int idx) {

        if (idx == this.period) {
//            this.negDmPeriod.add(idx, Arrays.stream(Arrays.copyOfRange(this.negDM1, 0, this.period + 1)).sum());
            this.negDmPeriod.add(idx, this.negDM1.stream().filter(i -> i > 0 && i < this.period + 1).mapToDouble(i -> i).sum());
        } else if (idx > this.period) {
            double prevNegDmPeriod = this.negDmPeriod.get(idx - 1);
            this.negDmPeriod.add(idx, prevNegDmPeriod - (prevNegDmPeriod / this.period) + this.negDM1.get(idx - 1));
        } else {
            this.negDmPeriod.add(idx, 0.00);
        }

    }

    private void positiveDirectionalIndicator(int idx) {

        double posDmPeriod = this.posDmPeriod.get(idx);
        double trPeriod = this.trPeriod.get(idx);

        if (trPeriod > 0)
            this.posDiPeriod.add(idx, 100 * (posDmPeriod / trPeriod));
        else
            this.posDiPeriod.add(idx, 0.00);
    }

    private void negativeDirectionalIndicator(int idx) {
        double negDmPeriod = this.negDmPeriod.get(idx);
        double trPeriod = this.trPeriod.get(idx);

        if (trPeriod > 0)
            this.negDiPeriod.add(idx, 100 * (negDmPeriod / trPeriod));
        else
            this.negDiPeriod.add(idx, 0.00);
    }

    private void directionalDiffPeriod(int idx) {
        this.diDiffPeriod.add(idx, Math.abs(this.posDiPeriod.get(idx) - this.negDiPeriod.get(idx)));
    }

    private void directionalSumPeriod(int idx) {
        this.diSumPeriod.add(idx, Math.abs(this.posDiPeriod.get(idx) + this.negDiPeriod.get(idx)));
    }

    private void directionalIndex(int idx) {
        double diDiffPeriod = this.diDiffPeriod.get(idx);
        double diSumPeriod = this.diSumPeriod.get(idx);

        if (diSumPeriod > 0)
            this.dx.add(idx, 100 * (diDiffPeriod / diSumPeriod));
        else
            this.dx.add(idx, 0.00);
    }

    private void averageDirectionalIndex(int idx) {

        if (idx == (this.period + (this.period - 1))) {

            double[] data = new double[this.period];
            for (int i = 0; i < this.period; i++) {
                int currentIdx = idx - i;
                data[i] = this.dx.get(currentIdx);
            }

            adx.add(idx, Arrays.stream(data).average().getAsDouble());

        } else if (idx > (this.period + (this.period - 1))) {
            adx.add(idx, ((this.adx.get(idx - 1) * (this.period - 1)) + this.dx.get(idx)) / this.period);
        } else {
            adx.add(idx, 0.00);
        }

    }
}
//package haema;
//
//import build.common.Candle;
//import build.common.CandleProperty;
//import build.technicalindicator.TechnicalIndicator;
//import build.technicalindicator.TechnicalIndicatorHelper;
//
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//
//public class ADX implements TechnicalIndicatorHelper {
//    private final int periodLength;
//    private double[] tr = null;
//    private double[] dmPlus = null;
//    private double[] dmMinus = null;
//    private double[] trN = null;
//    private double[] dmPlusN = null;
//    private double[] dmMinusN = null;
//    private double[] dx = null;
//    public double[] adx = null;
//    private int counter = 0;
//    double diPlus = 0;
//    double diMinus = 0;
//    List<Candle> histBars;
//    public List<Double> diPlusList;
//    public List<Double> diMinusList;
//
//    public ADX(final List<Candle> qh, final int periodLength) {
//        histBars = qh;
//        diPlusList = new ArrayList<>();
//        diMinusList = new ArrayList<>();
//        this.periodLength = periodLength;
//        this.tr = new double[periodLength];
//        this.dmPlus = new double[periodLength];
//        this.dmMinus = new double[periodLength];
//        this.trN = new double[periodLength];
//        this.dmPlusN = new double[periodLength];
//        this.dmMinusN = new double[periodLength];
//        this.dx = new double[periodLength];
//        this.adx = new double[periodLength];
//    }
//
//    public void calculate() {
//
//        // int periodStart = qh.size() - periodLength;
//        int periodEnd = histBars.size() - 1;
//        double high = histBars.get(histBars.size() - 1).getHigh();
//        double low = histBars.get(histBars.size() - 1).getLow();
//        // double close histBars qh.getLastPriceBar().getClose();
//        double high_1 = histBars.get(histBars.size() - 2).getHigh();
//        double low_1 = histBars.get(histBars.size() - 2).getLow();
//        double close_1 = histBars.get(histBars.size() - 2).getClose();
//
//        for (int i = 0; i < periodLength - 1; i++) {
//            tr[i] = tr[i + 1];
//            dmPlus[i] = dmPlus[i + 1];
//            dmMinus[i] = dmMinus[i + 1];
//            trN[i] = trN[i + 1];
//            dmPlusN[i] = dmPlusN[i + 1];
//            dmMinusN[i] = dmMinusN[i + 1];
//            dx[i] = dx[i + 1];
//            adx[i] = adx[i + 1];
//        }
//
//        // the first calculation for ADX is the true range value (TR)
//        tr[periodLength - 1] = Math.max(high - low, Math.max(Math.abs(high
//                - close_1), Math.abs(low - close_1)));
//
//        // determines the positive directional movement or returns zero if there
//        // is no positive directional movement.
//        dmPlus[periodLength - 1] = high - high_1 > low_1 - low ? Math.max(high
//                - high_1, 0) : 0;
//
//        // calculates the negative directional movement or returns zero if there
//        // is no negative directional movement.
//        dmMinus[periodLength - 1] = low_1 - low > high - high_1 ? Math.max(
//                low_1 - low, 0) : 0;
//
//        // The daily calculations are volatile and so the data needs to be
//        // smoothed. First, sum the last N periods for TR, +DM and - DM
//        double trSum = 0;
//        double dmPlusSum = 0;
//        double dmMinusSum = 0;
//        for (int i = 0; i < periodLength; i++) {
//            trSum += tr[i];
//            dmPlusSum += dmPlus[i];
//            dmMinusSum += dmMinus[i];
//        }
//
//        // The smoothing formula subtracts 1/Nth of yesterday's trN from
//        // yesterday's trN and then adds today's TR value
//        // The truncating function is used to calculate the indicator as close
//        // as possible to the developer of the ADX's original form of
//        // calculation (which was done by hand).
//        trN[periodLength - 1] = ((int) (1000 * (trN[periodLength - 2]
//                - (trN[periodLength - 2] / periodLength) + trSum))) / 1000.0;
//        dmPlusN[periodLength - 1] = ((int) (1000 * (dmPlusN[periodLength - 2]
//                - (dmPlusN[periodLength - 2] / periodLength) + dmPlusSum))) / 1000.0;
//        dmMinusN[periodLength - 1] = ((int) (1000 * (dmMinusN[periodLength - 2]
//                - (dmMinusN[periodLength - 2] / periodLength) + dmMinusSum))) / 1000.0;
//
//        // Now we have a 14-day smoothed sum of TR, +DM and -DM.
//        // The next step is to calculate the ratios of +DM and -DM to TR.
//        // The ratios are called the +directional indicator (+DI) and
//        // -directional indicator (-DI).
//        // The integer function (int) is used because the original developer
//        // dropped the values after the decimal in the original work on the ADX
//        // indicator.
//        diPlus = (100 * dmPlusN[periodLength - 1] / trN[periodLength - 1]);
//        diMinus = (100 * dmMinusN[periodLength - 1] / trN[periodLength - 1]);
//        System.out.println("Plus " + diPlus);
//        System.out.println("Neg " + diMinus);
//
//
//        // The next step is to calculate the absolute value of the difference
//        // between the +DI and the -DI and the sum of the +DI and -DI.
//        double diDiff = Math.abs(diPlus - diMinus);
//        double diSum = diPlus + diMinus;
//
//        // The next step is to calculate the DX, which is the ratio of the
//        // absolute value of the difference between the +DI and the -DI divided
//        // by the sum of the +DI and the -DI.
//        dx[periodLength - 1] = (int) (100 * (diDiff / diSum));
//
//        // The final step is smoothing the DX to arrive at the value of the ADX.
//        // First, average the last N days of DX values
//        double dxMedia = 0;
//        for (int i = 0; i < periodLength; i++) {
//            dxMedia += dx[i];
//        }
//        dxMedia /= periodLength;
//        System.out.println("ADX: " + dxMedia);
//        // The smoothing process uses yesterday's ADX value multiplied by N-1,
//        // and then add today's DX value. Finally, divide this sum by N.
//        if (counter == 2 * (periodLength - 1)) {
//            adx[periodLength - 2] = dxMedia;
//        }
//        adx[periodLength - 1] = (adx[periodLength - 2] * (periodLength - 1) + dx[periodLength - 1])
//                / periodLength;
//
//        counter++;
//
////		value = adx[periodLength - 1];
////		return value;
//    }
//
//    public double getAdx() {
//        return adx[periodLength - 1];
//    }
//
//    public double getPosDI() {
//        return diPlus;
//    }
//
//    public double getNegDI() {
//        return diMinus;
//    }
//
//    @Override
//    public boolean initialized() {
//        return false;
//    }
//
//    @Override
//    public void initialize(Iterator<Candle> iterator) {
//
//    }
//
//    @Override
//    public void update(Candle candle) {
//
//    }
//
//    @Override
//    public void update(double v) {
//
//    }
//
//    @Override
//    public double get() {
//        return 0;
//    }
//
//    @Override
//    public void reset() {
//
//    }
//
//    @Override
//    public void setCandleProperty(CandleProperty candleProperty) {
//
//    }
//}