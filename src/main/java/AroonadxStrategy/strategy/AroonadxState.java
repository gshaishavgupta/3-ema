package AroonadxStrategy.strategy;
import AroonadxStrategy.AroonadxSummary;
import build.common.Candle;
import build.common.CandleProperty;
import build.common.HistDataDownloadRequest;
import build.common.Instrument;
import build.strategy.TimerData;
import enums.EXCHANGE;
import build.technicalindicator.TechnicalIndicator;
import AroonadxStrategy.RequestAdjustedHistData;
import build.utils.ArqLogger;
import enums.INDICATOR_TYPE;
import AroonadxStrategy.ADX;
import AroonadxStrategy.Aroon;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.lang.Double;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.util.logging.Level.INFO;
public class AroonadxState {
    AroonadxSummary summary;
    public static final int START_TIMER = 307;
    public boolean reachedStartTimer;
    TechnicalIndicator ema1;
    TechnicalIndicator ema2;
    TechnicalIndicator ema3;
    private boolean running;
    public long timeStamp;
    public final AroonadxStrategy m_strategy;
    private static final Logger m_logger;
    private static final DecimalFormat IDF;
    public static final int INTERVAL_TIMER;
    public Instrument instr;
    public Aroon aroon;
    private ADX adx;
    private final AtomicBoolean isCandlesDownloaded;
    public long token;
    List<Candle> pastcandles;
    public Boolean isSell;
    public Boolean isBuy;
    public double prevHighestHigh;
    public double prevLowestLow;
    public double prevBuyLow;
    public double prevBuyHigh;
    public double prevSellLow;
    public double prevSellHigh;
    SortedMap<Integer, AroonadxState> aroonadxCycle;
    static {
        IDF = new DecimalFormat("0.000000");
        m_logger = ArqLogger.getInstance().getLogger();
        INTERVAL_TIMER = 300;
    }
    AroonadxState(AroonadxSummary summ,AroonadxStrategy strategy)
    {
        m_strategy=strategy;
        summary = summ;
        reachedStartTimer = false;
        this.aroonadxCycle = new TreeMap<>();
        running = false;
        isSell=false;
        isBuy=false;
        prevHighestHigh=0;
        prevLowestLow=Double.MAX_VALUE;
        pastcandles=new ArrayList<>();
        isCandlesDownloaded = new AtomicBoolean(false);
        ema1=new TechnicalIndicator(INDICATOR_TYPE.EMA,summary.getEmaPeriod1());
        ema1.setCandleProperty(CandleProperty.CLOSE);
        ema2=new TechnicalIndicator(INDICATOR_TYPE.EMA,summary.getEmaPeriod2());
        ema2.setCandleProperty(CandleProperty.CLOSE);
        ema3=new TechnicalIndicator(INDICATOR_TYPE.EMA,summary.getEmaPeriod3());
        ema3.setCandleProperty(CandleProperty.CLOSE);


        TimerData timerData = new TimerData();
        timerData.id = START_TIMER;
        timerData.data = summary.getPortfolio();
        m_logger.log(Level.INFO, "Start Time in millis: " + summary.getStartTime() * 1000);
        m_logger.log(Level.INFO, "End Time: " + summary.getEndTime());
        long startDiff = summary.getStartTime() * 1000 - System.currentTimeMillis();
        m_logger.log(Level.INFO, "Difference b/w current and start time: " + startDiff);
        m_strategy.setTimer(timerData, (int) startDiff, 1, summary.getStartTime());

    }
    public void sendHistDataRequest(int interval, Date from, Date to) {
        HistDataDownloadRequest request = new HistDataDownloadRequest();
        request.continuous = false;
        request.from = from;
        request.to = to;
        request.interval = interval;
        request.token = token;
        request.listener = candles -> {
            try {
                this.onCandlesDownloaded(candles);
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
//        m_logger.log(INFO, "################# sendHistDataRequest " + from+"  "+to+"  "+interval);
        m_strategy.sendHistoricalDataRequest(request, summary.getBroker());
    }
    public boolean areCandlesDownloaded() {
        return isCandlesDownloaded.get();
    }

    private void onCandlesDownloaded(List<Candle> candles) {
//        m_logger.log(INFO, "################# onCandlesDownloaded");
//        m_logger.log(INFO, "################# " + candles.size());
//        m_logger.log(INFO, "################# from "+ candles.get(0).getTimeStamp() +" to "+ candles.get(candles.size()-1).getTimeStamp());
//        m_logger.log(INFO, "################# from "+ (candles.get(0).getTimeStamp() - candles.get(candles.size()-1).getTimeStamp()));
        isCandlesDownloaded.set(true);
        pastcandles = candles;
        aroon = new Aroon(summary.getAroonPeriod(), pastcandles);
        aroon.calculateAroonDown();
        aroon.calculateAroonUp();
        adx = new ADX(pastcandles);
        adx.calculate(summary.getAdxPeriod());
        if(ema1!=null)
            ema1.initialize(candles.iterator());
        if(ema2!=null)
            ema2.initialize(candles.iterator());
        if(ema3!=null)
            ema3.initialize(candles.iterator());
        m_logger.log(INFO, "called for Symbol"+summary.getSymbol());
        Date date = new Date(candles.get(candles.size() - 1).getTimeStamp() * 1000L);
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("Asia/IST"));

        String formatted = format.format(date);
        m_logger.log(INFO, "First Candle for Symbol "+summary.getSymbol()+"  :" +
                formatted +
                ", Last Candle: " + new Date(candles.get(candles.size() - 1).getTimeStamp()));

        TimerData timerdata1 = new TimerData();
        timerdata1.id = INTERVAL_TIMER;
        ZonedDateTime ldate = LocalDateTime.parse(formatted, DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"))
                .atZone(ZoneId.of("Asia/Calcutta"));
        timeStamp = ldate.toInstant().toEpochMilli();
        m_logger.log(Level.INFO, "Actual timestamp: " + timeStamp);
        timerdata1.data = summary.getPortfolio();
        long actualStartTime = timeStamp +
                TimeUnit.MINUTES.toMillis(summary.getInterval()) + 1000;
        m_logger.log(Level.INFO, "Actual Start time: " + actualStartTime);
        m_strategy.setTimer(timerdata1, 60 * summary.getInterval(), -1, actualStartTime);

    }
    public void onLatestCandle(Candle candle) {
        Date date = new Date(candle.getTimeStamp() * 1000L);
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("Asia/IST"));
        String formatted = format.format(date);
        m_logger.log(Level.INFO, "New date: " + formatted +
                " OPEN: " + candle.getOpen() +
                " HIGH: " + candle.getHigh() +
                " LOW: " + candle.getLow() +
                " CLOSE: " + candle.getClose());
        pastcandles.add(candle);

        if (ema1 != null) {
            ema1.update(candle);
        }

        if (ema2 != null) {
            ema2.update(candle);
        }
        if (ema3 != null) {
            ema3.update(candle);
        }

        m_logger.log(INFO,"Updated EMA 1 Value for Symbol "+summary.getSymbol()+"   : "+ema1.get());
        m_logger.log(INFO,"Updated EMA 2 Value for Symbol "+summary.getSymbol()+"   : "+ema2.get());
        m_logger.log(INFO,"Updated EMA 3 Value for Symbol "+summary.getSymbol()+"   : "+ema3.get());
    }

    public Boolean isBuyCondition()
    {
        m_logger.log(INFO,"############ isBuyCondition called ");
        adx = new ADX(pastcandles);
        aroon = new Aroon(summary.getAroonPeriod(), pastcandles);
        adx.calculate(summary.getAdxPeriod());
        List<Double> adxValue = adx.getADX();
        double newADX=adxValue.get(adxValue.size()-1);
        m_logger.log(INFO,"Updated ADX Value for Symbol "+summary.getSymbol()+"  : "+ newADX);
        List<Double> posDIValue = adx.getPositiveDirectionalIndicator();
        double newposDI=posDIValue.get(posDIValue.size()-1);
        m_logger.log(INFO,"Updated positive DI Value for Symbol "+summary.getSymbol()+"  : "+ newposDI);
        List<Double> negDIValue = adx.getNegativeDirectionalIndicator();
        double newnegDI=negDIValue.get(negDIValue.size()-1);
        m_logger.log(INFO,"Updated negative DI Value for Symbol "+summary.getSymbol()+"  : "+ newnegDI);
        List<Double> aroonUp = aroon.calculateAroonUp();
        int aroonPrevCandleIndex= aroonUp.size()-2;
        int aroonCurrCandleIndex=aroonUp.size()-1;
        m_logger.log(INFO,"Updated aroonUp Value for Symbol "+summary.getSymbol()+"  : "+ aroonUp.get(aroonCurrCandleIndex));
//        m_logger.log(INFO," ################ isBuyCondition ema2.get()<ema1.get() "+(ema2.get()<ema1.get())+"  ");
//        m_logger.log(INFO," ################ isBuyCondition ema3.get()<ema2.get() "+(ema3.get()<ema2.get())+"  ");
//        m_logger.log(INFO," ################ ema1.get() "+ema1.get()+" ema2.get() "+ema2.get()+" ema3.get() "+ema3.get());
//        m_logger.log(INFO," ################ isBuyCondition newADX<18 "+(newADX<18)+"  "+newADX);
//        m_logger.log(INFO," ################ isBuyCondition newnegDI<newposDI "+(newnegDI<newposDI)+"  ");
//        m_logger.log(INFO," ################ isBuyCondition aroonUp.get(aroonCurrCandleIndex)==100 "+(aroonUp.get(aroonCurrCandleIndex)==100)+"  "+
//                aroonUp.get(aroonCurrCandleIndex));
//        m_logger.log(INFO," ################ isBuyCondition aroonUp.get(aroonPrevCandleIndex)<30 "+(aroonUp.get(aroonPrevCandleIndex)<30)+"  "+
//                aroonUp.get(aroonPrevCandleIndex));

        if(ema3.get()<ema2.get()
                &&ema2.get()<ema1.get()
                &&newADX<18
                &&newnegDI<newposDI
                &&aroonUp.get(aroonPrevCandleIndex)<30&&aroonUp.get(aroonCurrCandleIndex)==100)
        {
            m_logger.log(INFO,"Buy Condtion True for Symbol "+summary.getSymbol()+" !");
            isBuy=true;
            return true;
        }
        return false;
    }
    public Boolean isSellCondition()
    {
        m_logger.log(INFO,"############ isSellCondition called ");
        adx = new ADX(pastcandles);
        aroon = new Aroon(summary.getAroonPeriod(), pastcandles);
        adx.calculate(summary.getAdxPeriod());
        List<Double> adxValue = adx.getADX();
        double newADX=adxValue.get(adxValue.size()-1);
        m_logger.log(INFO,"Updated ADX Value for Symbol "+summary.getSymbol()+"  : "+ newADX);
        List<Double> posDIValue = adx.getPositiveDirectionalIndicator();
        double newposDI=posDIValue.get(posDIValue.size()-1);
        m_logger.log(INFO,"Updated posDI Value for Symbol "+summary.getSymbol()+"  : "+ newposDI);
        List<Double> negDIValue = adx.getNegativeDirectionalIndicator();
        double newnegDI=negDIValue.get(negDIValue.size()-1);
        m_logger.log(INFO,"Updated negDI Value for Symbol "+summary.getSymbol()+"  : " + newnegDI);
        List<Double> aroonDown = aroon.calculateAroonDown();
        int aroonPrevCandleIndex= aroonDown.size()-2;
        int aroonCurrCandleIndex=aroonDown.size()-1;

//        m_logger.log(INFO," ################ isSellCondition ema2.get()>ema1.get() "+(ema2.get()>ema1.get())+"  ");
//        m_logger.log(INFO," ################ isSellCondition ema3.get()>ema2.get() "+(ema3.get()>ema2.get())+"  ");
//        m_logger.log(INFO," ################ ema1.get() "+ema1.get()+" ema2.get() "+ema2.get()+" ema3.get() "+ema3.get());
//        m_logger.log(INFO," ################ isSellCondition newADX<18 "+newADX+"  "+(newADX<18)+"  ");
//        m_logger.log(INFO," ################ isSellCondition newnegDI>newposDI "+(newnegDI>newposDI)+"  ");
//        m_logger.log(INFO," ################ isSellCondition newnegDI "+newnegDI+" newposDI "+newposDI+"  ");
//        m_logger.log(INFO," ################ isSellCondition aroonDown.get(aroonCurrCandleIndex)==100 "+(aroonDown.get(aroonCurrCandleIndex)==100)+"  "+
//                aroonDown.get(aroonCurrCandleIndex));
//        m_logger.log(INFO," ################ isSellCondition aroonDown.get(aroonPrevCandleIndex)<30 "+(aroonDown.get(aroonPrevCandleIndex)<30)+"  "+
//                aroonDown.get(aroonPrevCandleIndex));

        if(ema3.get()>ema2.get()
                &&ema2.get()>ema1.get()
                &&newADX<18
                &&newnegDI>newposDI
                &&aroonDown.get(aroonPrevCandleIndex)<30&&aroonDown.get(aroonCurrCandleIndex)==100)
        {
            m_logger.log(INFO,"Sell Condtion True for Symbol "+summary.getSymbol()+" !");
            isSell=true;
            return true;
        }
        return false;
    }
    public void checkCrossover(Candle candle) {
        m_logger.log(INFO," checkCrossover");
        if (!running)
            return;

        if (isBuyCondition())
        {
            m_logger.log(INFO," is buy true");
            Objects.requireNonNull(m_strategy.getMktData(summary.getSymbol()));
            prevBuyHigh=candle.getHigh();
            prevBuyLow=candle.getLow();
            setStoploss(true);
        }

        if (isSellCondition())
        {
            m_logger.log(INFO," is sell true");
            Objects.requireNonNull(m_strategy.getMktData(summary.getSymbol()));
            prevSellLow=candle.getLow();
            prevSellHigh=candle.getHigh();
            setStoploss(false);
        }
        m_logger.log(INFO," checkCrossover finished");
    }
    public void setStoploss(boolean isBuy) {
        m_logger.log(INFO," SET STOPLOSS CALLED ");
        for (int i = pastcandles.size() - 1; i > pastcandles.size() - 6; i--) {
            if (!isBuy) {
                if (Double.compare(pastcandles.get(i).getHigh(), prevHighestHigh) > 0) {
                    prevHighestHigh = pastcandles.get(i).getHigh();
                }
            } else {
                if (Double.compare(pastcandles.get(i).getLow(), prevLowestLow) < 0) {
                    prevLowestLow = pastcandles.get(i).getLow();
                }
            }
        }
    }
    public void writeToCSV(List<Candle> data) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(new File("HistData.csv")));

            writer.write("OPEN,HIGH,LOW,CLOSE,VOLUME,DATE,TIME");
            writer.newLine();

            for (Candle candle : data) {

                Date date = new Date(candle.getTimeStamp());

                writer.write(
                        IDF.format(candle.getOpen()) + "," +
                                IDF.format(candle.getHigh()) + "," +
                                IDF.format(candle.getLow()) + "," +
                                IDF.format(candle.getClose()) + "," +
                                IDF.format(candle.getVolume()) + "," +
                                dateFormat.format(date) + "," +
                                timeFormat.format(date));

                writer.newLine();
                writer.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void start() {
        running = true;
    }

    public void pause() {
        running = false;
    }

}

