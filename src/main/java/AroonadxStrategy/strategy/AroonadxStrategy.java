package AroonadxStrategy.strategy;
import AroonadxStrategy.AroonadxSummary;
import build.common.*;
import build.strategy.*;
import build.utils.ArqLogger;
import build.common.HistDataDownloadRequest;
import AroonadxStrategy.RequestAdjustedHistData;
import build.utils.StandardTimes;
import AroonadxStrategy.Aroon;
import enums.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.INFO;

public class AroonadxStrategy extends AbstractStrategy {
    protected final HashMap<String, AroonadxState> m_mapStates;
    protected static final Logger m_logger = ArqLogger.getInstance().getLogger();
    public AroonadxStrategy() {
        super("AROONADX");
        m_mapStates = new HashMap<>();
    }
    @Override
    public void onBrokerLoginComplete(String s) {

    }
    @Override
    public void onBrokerLogoutComplete(String s) {

    }
    @Override
    public void onBrokerMessage(BrokerMessage brokerMessage) {

    }
    @Override
    public void onTimer(TimerData timerData) {
        if (timerData.id == AroonadxState.START_TIMER) {
            String portfolio = (String) timerData.data;
            AroonadxState state = m_mapStates.get(portfolio);

            if (state == null) {
                m_logger.log(Level.SEVERE, "Aroonadx State is null for Symbol "+state.summary.getSymbol()+" !");
            }

            state.instr = getFirstInstrument(i ->
                    i.getSymbol().equals(state.summary.getSymbol())
                            && i.getExchange().equals(EXCHANGE.NSE));
            state.token = state.instr.getToken();
            m_logger.log(INFO, "Token: " + state.token);
            int compVal = 0;
            switch (state.summary.getInterval()) {
                case 1 -> compVal = 60;
                case 3 -> compVal = 180;
                case 5 -> compVal = 300;
                case 10 -> compVal = 600;
                case 15 -> compVal = 900;
                case 30 -> compVal = 1800;
                case 60 -> compVal = 3600;
            }

            state.sendHistDataRequest(compVal, RequestAdjustedHistData.getHistoricalStartDate(state.summary.getInterval()),
                    RequestAdjustedHistData.getHistoricalEndDate(state.summary.getCreatedTime(),
                            state.summary.getInterval(), state.instr.getExchange().name()));

            state.reachedStartTimer = true;
            m_logger.log(Level.SEVERE, "reached start timer "+state.reachedStartTimer);
        }

        if (timerData.id == AroonadxState.INTERVAL_TIMER) {
        String portfolio = (String) timerData.data;
        AroonadxState state = m_mapStates.get(portfolio);
        if (state == null) {
            m_logger.log(Level.SEVERE, "State is null, can't update indicators for Symbol "+state.summary.getSymbol()+" !");
            return;
        }
        if(state.reachedStartTimer) {
            m_logger.log(Level.INFO, "INTERVAL TIMER called for Symbol " + state.summary.getSymbol() + " !");
            HistDataDownloadRequest request = new HistDataDownloadRequest();
            request.token = state.token;
            int compVal = 0;
            switch (state.summary.getInterval()) {
                case 1 -> compVal = 60;
                case 3 -> compVal = 180;
                case 5 -> compVal = 300;
                case 10 -> compVal = 600;
                case 15 -> compVal = 900;
                case 30 -> compVal = 1800;
                case 60 -> compVal = 3600;
            }
            request.interval = compVal;
            int interval = state.summary.getInterval();
            long minutes = ChronoUnit.MINUTES.between(StandardTimes.NSE_START_TIME, LocalTime.now());
            long roundedMinutes = interval * (minutes / interval);
            LocalTime toTime = StandardTimes.NSE_START_TIME.plusMinutes(roundedMinutes);
            LocalTime fromTime = toTime.minusMinutes(interval);
            request.to = new Date(
                    toTime.toEpochSecond(LocalDate.now(), OffsetDateTime.now().getOffset())
                            * 1000L
            );
            request.from = new Date(
                    fromTime.toEpochSecond(LocalDate.now(), OffsetDateTime.now().getOffset())
                            * 1000L
            );
            request.continuous = false;
            request.listener = candles -> {
                int s = candles.size();
                if (s > 2) {
                    m_logger.log(Level.WARNING, "Expecting one candle, got more than 2 for Symbol " + state.summary.getSymbol() + " !");
                } else if (s > 0) {
                    this.onLatestCandleDownloaded(state, candles.get(0));
                } else {
                    m_logger.log(Level.SEVERE, "Invalid number of candles for Symbol " + state.summary.getSymbol() + "  :" + s);
                }
            };

            m_logger.log(Level.INFO, "HistDataReq: Token += " + request.token
                    + ", interval=" + request.interval + ", from=" + request.from.toString()
                    + ", to=" + request.to.toString() + ", continuous=" + request.continuous + " for Symbol " + state.summary.getSymbol());
            sendHistoricalDataRequest(request, "XTS");
        }
    }

}
    private void onLatestCandleDownloaded(AroonadxState state, Candle candle) {

        m_logger.log(Level.SEVERE, "onLatestCandleDownloaded called ");

        if (!state.summary.getStatus().equals(STRATEGY_STATUS.STOPPED))
            state.onLatestCandle(candle);

        state.checkCrossover(candle);
    }

    private  void squareOff(AroonadxState state)
    {
        Positions pos = getPositions(state.summary.getPortfolio(),state.summary.getSymbol());
        int x = pos.getNetQty();
        MktData ordrMktData = getMktData(state.summary.getSymbol());
        Instrument instrument = getFirstInstrument(
                (instr) -> instr.getSymbol().equals(state.summary.getSymbol()));
        if(x > 0)
        {
            OrderRequest request = createOrderRequest(state.summary.getPortfolio(), state.summary.getSymbol(), (int) Math.floor(state.summary.getMargin() / ordrMktData.getLast()),
                     ORDER_SIDE.SELL, ORDER_STYLE.SQUAREOFF, API_ORDER_TYPE.MIS, ORDER_TYPE.MARKET);
//            OrderRequest request = createOrderRequest(state.summary.getSymbol(),ORDER_SIDE.SELL,ORDER_STYLE.ENTRY,ORDER_TYPE.MARKET,
//                    state.summary.getPortfolio(), x*-1);
            ORDER_ERROR_CODE ord = sendOrder(request);
            if (ord != ORDER_ERROR_CODE.SUCCESS) {
                m_logger.log(Level.INFO, "Sell order not sent for symbol " + state.summary.getSymbol() +" order code: " + ord+" ! "+state.summary.getPortfolio());
            }
        }
        else if(x < 0)
        {
//            OrderRequest request = createOrderRequest(state.summary.getSymbol(),ORDER_SIDE.BUY,ORDER_STYLE.ENTRY,ORDER_TYPE.MARKET,
//                    state.summary.getPortfolio(), x*-1);
            OrderRequest request = createOrderRequest(state.summary.getPortfolio(), state.summary.getSymbol(), (int) Math.floor(state.summary.getMargin() / ordrMktData.getLast()),
                    ORDER_SIDE.SELL, ORDER_STYLE.SQUAREOFF, API_ORDER_TYPE.MIS, ORDER_TYPE.MARKET);
            ORDER_ERROR_CODE ord = sendOrder(request);
            if (ord != ORDER_ERROR_CODE.SUCCESS) {
                m_logger.log(Level.INFO, "Buy order not sent for symbol " + state.summary.getSymbol() +" order code: " + ord+" ! "+state.summary.getPortfolio());
            }
        }
        m_logger.log(Level.SEVERE, "stoploss hit for Symbol "+state.summary.getSymbol()+" !");
    }
    public Boolean checkExitCondition(AroonadxState state,MktData mktData)
    {

        state.aroon = new Aroon(state.summary.getAroonPeriod(), state.pastcandles);
        List<Double> aroonDown=state.aroon.calculateAroonDown();
        List<Double> aroonUp=state.aroon.calculateAroonUp();
        if((state.isBuy&&mktData.getLast()>state.prevBuyLow&&Double.compare(aroonDown.get(aroonDown.size()-1),100)>=0)||
                (state.isSell&&mktData.getLast()<state.prevSellHigh&&Double.compare(aroonUp.get(aroonUp.size()-1),100)>=0))
        {
            m_logger.log(Level.SEVERE, "Exit Condition is true for Symbol "+state.summary.getSymbol()+" !");

            return true;
        }
        return false;
    }
    @Override
    public void onData(MktData mktData) {
        m_mapStates.values().forEach(state ->{
            state.summary.setSymbolLtp(mktData.getLast());
            if(state.isBuy)
            {
//                m_logger.log(INFO," @@@@@@@@@@@@@@@@ is buy true");
                if(Double.compare(mktData.getLast(),state.prevBuyHigh)>0) {
                    sendBuyOrder(state.summary);
                    state.prevBuyHigh= Double.MAX_VALUE;
                }

                if(mktData.getLast()<state.prevLowestLow) {
                    squareOff(state);//stoploss hit
                    state.prevLowestLow = 0;
                    state.isBuy = false;
                }
                else if(checkExitCondition(state,mktData)) {
                    squareOff(state);//exit codn true;
                    state.isBuy = false;
                }
            }
            if(state.isSell) {
//                m_logger.log(INFO," @@@@@@@@@@@@@@@@ is sell true");
                if (Double.compare(mktData.getLast(), state.prevSellLow) < 0) {
                    sendSellOrder(state.summary);
                    state.prevSellLow = 0;
                }
                if (mktData.getLast() < state.prevHighestHigh) {
                    squareOff(state);
                    state.isSell = false;
                    state.prevHighestHigh = Integer.MAX_VALUE;
                }
                else if(checkExitCondition(state,mktData)) {
                    squareOff(state);
                    state.isSell = false;
                }
            }

        });
    }

    @Override
    public void onOrderPendingNew(Order order) {

    }

    @Override
    public void onOrderAck(Order order) {

    }

    @Override
    public void onOrderPendingReplace(Order order) {

    }

    @Override
    public void onOrderReplaced(Order order) {

    }

    @Override
    public void onOrderPendingCancel(Order order) {

    }

    @Override
    public void onOrderCancelled(Order order) {

    }

    @Override
    public void onOrderRejected(Order order) {

    }

    @Override
    public void onReplaceRejected(Order order) {

    }

    @Override
    public void onCancelRejected(Order order) {

    }

    @Override
    public void onOrderPartFill(Order order, int i, double v) {

    }

    @Override
    public void onOrderFilled(Order order, int i, double v) {


    }

    @Override
    public void onStopLossTriggered(Order order) {

    }

    @Override
    public void onUserAction(UserActionData userActionData) {

    }

    @Override
    public boolean onPortfolioRecover(StrategyParams strategyParams) {
        AroonadxSummary summary = (AroonadxSummary) strategyParams;
        AroonadxState state = new AroonadxState(summary, this);

        if(summary.getStatus()!=STRATEGY_STATUS.INVALID) {
            List<Order> allOrders = new ArrayList<>();
            allOrders.addAll(getAllOrders(summary.getPortfolio(), summary.getSymbol(), ORDER_SIDE.BUY));
            allOrders.addAll(getAllOrders(summary.getPortfolio(), summary.getSymbol(), ORDER_SIDE.SELL));

            for (Order ord : allOrders) {
                long l = ord.getOrderSendTime() * 1000 - summary.getInterval() * 5;
                //l -= (l % 60000L);
                Candle candle = RecoverStopLoss(summary, state, l);
                if (ord.isRecovered()) {
                    if (ord.getStyle() == ORDER_STYLE.ENTRY) {
                        MktData mktData = getMktData(summary.getSymbol());
                        squareOff(state);
                        if (ord.getOrderSide().equals(ORDER_SIDE.BUY)) {
                            Objects.requireNonNull(state.m_strategy.getMktData(summary.getSymbol()));
                            state.isBuy = true;
                            state.prevBuyHigh = candle.getHigh();
                            state.prevBuyLow = candle.getLow();
                            state.setStoploss(true);
                        } else if (ord.getOrderSide().equals(ORDER_SIDE.SELL)) {
                            Objects.requireNonNull(state.m_strategy.getMktData(summary.getSymbol()));
                            state.isSell = true;
                            state.prevSellLow = candle.getLow();
                            state.prevSellHigh = candle.getHigh();
                            state.setStoploss(false);
                        }
                    }
                }
            }
        }
        Objects.requireNonNull(state);
        m_mapStates.put(summary.getPortfolio(),state);
        subscribe(summary.getSymbol(), summary.getBroker());
        m_logger.log(Level.INFO, "Portfolio recovered for ID:"+summary.getPortfolio()+ " Symbol "+summary.getSymbol()+" StartTime "+summary.getStartTime()+" EndTime "+summary.getEndTime()+
                " EMA Period 1 "+summary.getEmaPeriod1()+" EMA Period 2 "+summary.getEmaPeriod2()+" EMA Period 3 " +summary.getEmaPeriod3()+
                " Aroon Period "+summary.getAroonPeriod()+" ADX Period "+summary.getAdxPeriod()+" Interval "+summary.getInterval());

        return true;
    }

    public Candle RecoverStopLoss(AroonadxSummary summary,AroonadxState state,long sendTime)
    {
        int compVal = 0;
        switch (summary.getInterval()) {
            case 1 -> compVal = 60;
            case 3 -> compVal = 180;
            case 5 -> compVal = 300;
            case 10 -> compVal = 600;
            case 15 -> compVal = 900;
            case 30 -> compVal = 1800;
            case 60 -> compVal = 3600;
        }
        state.sendHistDataRequest(compVal, RequestAdjustedHistData.getHistoricalStartDate(summary.getInterval()),
                RequestAdjustedHistData.getHistoricalEndDate(summary.getCreatedTime(),
                        summary.getInterval(), state.instr.getExchange().name()));
        Candle tcandle=null;
        int i=5;
        for(Candle candle:state.pastcandles)
        {
            if(candle.getTimeStamp()<sendTime)
            {
                if(i==0)
                {
                    tcandle=candle;
                    break;
                }
                i--;
            }
        }
    return tcandle;
    }

    @Override
    public boolean onPortfolioCreate(StrategyParams strategyParams)
    {
        AroonadxSummary summary = (AroonadxSummary) strategyParams;
        AroonadxState state = new AroonadxState(summary,this);
        Objects.requireNonNull(state);
        m_mapStates.put(summary.getPortfolio(),state);
        subscribe(summary.getSymbol(),summary.getBroker());
        m_logger.log(Level.INFO, "Portfolio Created for Symbol "+summary.getSymbol()+" StartTime "+summary.getStartTime()+" EndTime "+summary.getEndTime()+
                " EMA Period 1 "+summary.getEmaPeriod1()+" EMA Period 2 "+summary.getEmaPeriod2()+" EMA Period 3 " +summary.getEmaPeriod3()+
                " Aroon Period "+summary.getAroonPeriod()+" ADX Period "+summary.getAdxPeriod()+" Interval "+summary.getInterval());
        return true;
    }

    @Override
    public boolean onPortfolioModify(StrategyParams strategyParams) {
        AroonadxSummary summary =(AroonadxSummary) strategyParams ;

        final AroonadxState state = new AroonadxState(summary,this);
        m_mapStates.putIfAbsent(summary.getPortfolio(), state);

        return true;
    }

    @Override
    public boolean onPortfolioDelete(StrategyParams strategyParams) {
        AroonadxSummary summary = (AroonadxSummary) strategyParams;
        m_logger.log(Level.INFO, "AroonadxStrategy: Deleting portfolio:- " + summary.getPortfolio());
        return true;
    }

    @Override
    public boolean onPortfolioStart(StrategyParams strategyParams) {

        AroonadxSummary summary = (AroonadxSummary)strategyParams;
        AroonadxState state = m_mapStates.get(summary.getPortfolio());
        if (!state.areCandlesDownloaded()) {
            m_logger.log(Level.SEVERE, "Candles not downloaded yet for Symbol "+state.summary.getSymbol()+" !");
        }

        m_logger.log(Level.INFO, "AroonnadxStrategy::Start "
                + summary.getPortfolio() + ":"
                + summary.getStrategy());
        state.summary.setStatus(STRATEGY_STATUS.RUNNING);
        state.start();
        return true;
    }

    @Override
    public boolean onPortfolioStop(StrategyParams o) {
        AroonadxSummary row = (AroonadxSummary) o;
        AroonadxState state = m_mapStates.get(row.getPortfolio());
        if (state == null) {
            m_logger.log(Level.SEVERE, "Aroonadx State is null for Symbol "+state.summary.getSymbol()+" !");
            return false;
        }
        unsubscribe(row.getSymbol(), row.getBroker());

        m_logger.log(Level.INFO, "AroonadxStrategy::Stop "
                + row.getPortfolio() + ":"
                + row.getStrategy());
        state.pause();
        return true;
    }

    @Override
    public boolean onPortfolioPause(StrategyParams o) {
        AroonadxSummary summary = (AroonadxSummary) o;
        AroonadxState state = m_mapStates.get(summary.getPortfolio());
        if (state == null) {
            m_logger.log(Level.SEVERE, "Aroonadx State is null for Symbol "+state.summary.getSymbol()+" !");
            return false;
        }
        unsubscribe(summary.getSymbol(), summary.getBroker());

        m_logger.log(Level.INFO, "AroonadxStrategy::Pause "
                + summary.getPortfolio() + ":"
                + summary.getStrategy());
        state.pause();
        return true;
    }

    @Override
    public boolean onPortfolioStopSquareOff(StrategyParams strategyParams) {
        AroonadxSummary summary = (AroonadxSummary) strategyParams;
        AroonadxState state = m_mapStates.get(summary.getPortfolio());
        squareOff(state);
        summary.setStatus(STRATEGY_STATUS.STOPPED);
        state.pause();
        unsubscribe(summary.getSymbol(),summary.getBroker());
        return true;
    }

    @Override
    public boolean onPortfolioPauseSquareOff(StrategyParams strategyParams) {
        AroonadxSummary summary = (AroonadxSummary) strategyParams;
        AroonadxState state = m_mapStates.get(summary.getPortfolio());
        squareOff(state);
        summary.setStatus(STRATEGY_STATUS.STOPPED);
        state.pause();
        unsubscribe(summary.getSymbol(),summary.getBroker());
        return true;
    }

    @Override
    public void onPositionsUpdate(Positions positions) {

    }

    @Override
    public void onButtonColumnAction(String s, StrategyParams strategyParams) {

    }

    @Override
    public void onStrategyAction(String s, Object o) {

    }

    @Override
    public void onRecoveredOrder(Order order) {

    }

    @Override
    public OrderRequest getSquareOffOrderRequest(String s, String s1, ORDER_SIDE order_side, int i) {
        return null;
    }

    public void sendBuyOrder(AroonadxSummary summ)
    {
//        AroonadxState state = m_mapStates.get(summ.getPortfolio());
//        if (state == null) {
//            m_logger.log(Level.SEVERE, "Aroonadx State is null for Symbol "+state.summary.getSymbol()+" !");
//            return;
//        }
//        MktData ordrMktData = getMktData(summ.getSymbol());
//
//        OrderRequest request = createOrderRequest(summ.getSymbol(),ORDER_SIDE.BUY,ORDER_STYLE.ENTRY,ORDER_TYPE.MARKET,summ.getPortfolio(),
//                (int) Math.floor(summ.getMargin()/ordrMktData.getLast()));
//
//
//        m_logger.log(Level.SEVERE, " !!!!!!!!!!!! "+summ.getSymbol()+"  "+ORDER_SIDE.BUY+"  "+ORDER_STYLE.ENTRY+"  "+ORDER_TYPE.MARKET+
//                "  "+summ.getPortfolio()+"  "+(int) Math.floor(summ.getMargin()/ordrMktData.getLast()));
//        ORDER_ERROR_CODE status = sendOrder(request);
//        m_logger.log(Level.SEVERE, " ordrMktData.getLast() "+ordrMktData.getLast());
//        if(!status.equals(ORDER_ERROR_CODE.SUCCESS))
//            m_logger.log(Level.SEVERE, "Order not Bought for Symbol "+state.summary.getSymbol()+" ! "+status);
        AroonadxState state = m_mapStates.get(summ.getPortfolio());
        if (state == null) {
            m_logger.log(Level.SEVERE, "Aroonadx State is null for Symbol "+state.summary.getSymbol()+" !");
            return;
        }

        OrderRequest request = null;
        String ordrSymbol = summ.getSymbol();
        if (ordrSymbol != null) {
            MktData ordrMktData = getMktData(ordrSymbol);
            request = createOrderRequest(summ.getPortfolio(), summ.getSymbol(), (int) Math.floor(summ.getMargin()/ordrMktData.getLast()),
                        ORDER_SIDE.BUY, ORDER_STYLE.ENTRY, API_ORDER_TYPE.MIS, ORDER_TYPE.MARKET);

            ORDER_ERROR_CODE code = sendOrder(request);

            if (!code.equals(ORDER_ERROR_CODE.SUCCESS)) {
                m_logger.log(Level.SEVERE, "order not send "+code.name()+" ! "+summ.getPortfolio());
            }
            else
                m_logger.log(Level.SEVERE, "Order Sold for Symbol "+state.summary.getSymbol()+" !");
        }
    }

    private OrderRequest createOrderRequest(String portfolio, String symbol, int qty, ORDER_SIDE side,
                                            ORDER_STYLE style, API_ORDER_TYPE apiOrderType,
                                            ORDER_TYPE type) {
//        OrderRequest orderRequest = new OrderRequest();
//        orderRequest.instrument_token = getFirstInstrument(i->i.getSymbol().equals(symbol)).getToken();
//        orderRequest.portfolio = portfolio;
//        orderRequest.size = qty;
//        orderRequest.style = style;
//        orderRequest.broker = "XTS";
//        orderRequest.account = "TEST";
//        orderRequest.destination = EXCHANGE.NSE;
//        orderRequest.side = side;
//        orderRequest.type = type;
//        orderRequest.price = 50.0;
//        orderRequest.api_order_type = apiOrderType;
//        orderRequest.exec_trans_type = EXEC_TRANS_TYPE.NEW;
//        orderRequest.disclose_qty = qty;
//        orderRequest.tif = TIME_IN_FORCE.DAY;
//        orderRequest.portfolio_type = PORTFOLIO_TYPE.PAPER_TRADING;
//        orderRequest.strategy = "AROONADX";
//        return orderRequest;
        OrderRequest request = new OrderRequest();
        request.instrument_token = getFirstInstrument(i -> i.getSymbol().equals(symbol)).getToken();
        request.portfolio = portfolio;
        request.destination = EXCHANGE.NSE;
        request.portfolio_type = PORTFOLIO_TYPE.PAPER_TRADING;
        request.side = side;
        request.size = qty;
        request.exec_trans_type = EXEC_TRANS_TYPE.NEW;
        request.tif = TIME_IN_FORCE.DAY;
        request.style = style;
        request.type = type;
        request.account = "TEST";
        request.broker = "TEST";
//        request.broker = "XTS";
        request.price = 50.0;
        request.api_order_type = apiOrderType;
        request.disclose_qty = qty;
        request.strategy = "AROONADX";

        return request;
    }

//    public OrderRequest createOrderRequest(String symbol, ORDER_SIDE side,
//                                           ORDER_STYLE style, ORDER_TYPE type,
//                                           String portfolio, int size) {
//        OrderRequest request = new OrderRequest();
//        request.instrument_token = getFirstInstrument(i -> i.getSymbol().equals(symbol)).getToken();
//        request.portfolio = portfolio;
//        request.destination = EXCHANGE.NSE;
//        request.portfolio_type = PORTFOLIO_TYPE.PAPER_TRADING;
//        request.side = side;
//        request.size = size;
//        request.exec_trans_type = EXEC_TRANS_TYPE.NEW;
//        request.tif = TIME_IN_FORCE.DAY;
//        request.style = style;
//        request.type = type;
//        request.account = "TEST";
//        request.broker = "TEST";
//        request.price = 50.0;
//        request.api_order_type = API_ORDER_TYPE.NORMAL;
//        request.disclose_qty = size;
//        request.strategy = "AROONADX";
//
//        return request;
//    }

    public void sendSellOrder(AroonadxSummary summ)
    {
        AroonadxState state = m_mapStates.get(summ.getPortfolio());
        if (state == null) {
            m_logger.log(Level.SEVERE, "Aroonadx State is null for Symbol "+state.summary.getSymbol()+" !");
            return;
        }

        OrderRequest request = null;
        String ordrSymbol = summ.getSymbol();
        if (ordrSymbol != null) {
            MktData ordrMktData = getMktData(ordrSymbol);
            if (ordrMktData != null) {
                request = createOrderRequest(summ.getPortfolio(), summ.getSymbol(), (int) Math.floor(summ.getMargin() / ordrMktData.getLast()),
                        ORDER_SIDE.SELL, ORDER_STYLE.ENTRY, API_ORDER_TYPE.MIS, ORDER_TYPE.MARKET);
            }
            sendOrder(request);
            ORDER_ERROR_CODE code = sendOrder(request);

            if (!code.equals(ORDER_ERROR_CODE.SUCCESS)) {
                m_logger.log(Level.SEVERE, "order not send "+code.name()+" ! "+summ.getPortfolio());
            } else
                m_logger.log(Level.SEVERE, "Order Sold for Symbol " + state.summary.getSymbol() + " !");
//        AroonadxState state = m_mapStates.get(summ.getPortfolio());
//        if (state == null) {
//            m_logger.log(Level.SEVERE, "Aroonadx State is null for Symbol "+state.summary.getSymbol()+" !");
//            return;
//        }
//        MktData ordrMktData = getMktData(summ.getSymbol());
//
//        OrderRequest request = createOrderRequest(summ.getSymbol(),ORDER_SIDE.BUY,ORDER_STYLE.ENTRY,ORDER_TYPE.MARKET,summ.getPortfolio(),
//                (int) Math.floor(summ.getMargin()/ordrMktData.getLast()));
//
//
//        ORDER_ERROR_CODE status = sendOrder(request);
//        m_logger.log(Level.SEVERE, " ordrMktData.getLast() "+ordrMktData.getLast());
//        if(!status.equals(ORDER_ERROR_CODE.SUCCESS))
//            m_logger.log(Level.SEVERE, "Order not Bought for Symbol "+state.summary.getSymbol()+" ! "+status);
        }

    }
}
