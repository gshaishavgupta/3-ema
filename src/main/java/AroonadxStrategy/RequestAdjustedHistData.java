package AroonadxStrategy;

import build.utils.ArqLogger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RequestAdjustedHistData {
    private static final Logger m_logger = ArqLogger.getInstance().getLogger();
    private static Map<Integer, Integer> intervalToDays;
    static {
        intervalToDays = new HashMap<>();
        intervalToDays.put(1, 5);
        intervalToDays.put(3, 5);
        intervalToDays.put(5, 5);
        intervalToDays.put(10, 5);
        intervalToDays.put(15, 5);
        intervalToDays.put(30, 5);
        intervalToDays.put(60, 5);
    }

    public RequestAdjustedHistData() {

    }

    public static Date getHistoricalStartDate(int interval) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        int days = intervalToDays.get(interval);
        Date from = null;
        try {
            from = formatter.parse(LocalDate.now().minusDays(days).toString());
            m_logger.log(Level.INFO, "From Date: " + from);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return from;
    }

    public static Date getHistoricalEndDate(String createdTime, int interval, String exchange) {
        long now;
        m_logger.log(Level.INFO, "Interval: " + interval);
        Calendar calendar = Calendar.getInstance();
        LocalDateTime dt = null;
        Date date;
        Date yearDate;
        Date hoursAndMinutes;
        long lastCandleTime = TimeUnit.HOURS.toMinutes(calendar.get(Calendar.HOUR_OF_DAY)) +
                calendar.get(Calendar.MINUTE);
        lastCandleTime = lastCandleTime % interval;
        m_logger.log(Level.INFO, "Modulus: " + lastCandleTime);
        try {
            yearDate = new SimpleDateFormat("yyyy-MM-dd").parse(String.valueOf(LocalDateTime.now()));
            date = new SimpleDateFormat("hh:mm:ss a").parse(createdTime);
            hoursAndMinutes = new SimpleDateFormat("HH:mm").parse(date.getHours() + ":" +
                    date.getMinutes());
            LocalDate datePart = yearDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalTime timePart = hoursAndMinutes.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
            dt = LocalDateTime.of(datePart, timePart);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ZonedDateTime zdt = dt.atZone(ZoneId.of("Asia/Kolkata"));
        now = zdt.toInstant().toEpochMilli() - TimeUnit.MINUTES.toMillis(lastCandleTime);

        try {
            yearDate = new SimpleDateFormat("yyyy-MM-dd").parse(String.valueOf(LocalDateTime.now()));
            if (!exchange.startsWith("MCX")) {
                date = new SimpleDateFormat("HH:mm").parse("15:30");
            } else {
                date = new SimpleDateFormat("HH:mm").parse("23:30");
            }
            LocalDate datePart = yearDate.toInstant().atZone(ZoneId.of("Asia/Kolkata")).toLocalDate();
            LocalTime timePart = date.toInstant().atZone(ZoneId.of("Asia/Kolkata")).toLocalTime();
            dt = LocalDateTime.of(datePart, timePart);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ZonedDateTime zdt2 = dt.atZone(ZoneId.of("Asia/Kolkata"));
        long dayend = zdt2.toInstant().toEpochMilli();
        if (now > dayend) {
            now = dayend;
        }
        m_logger.log(Level.INFO, "End Time: " + new Date(now));
        return new Date(now);
    }
}
