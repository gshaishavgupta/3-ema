package AroonadxStrategy;


import build.strategy.StrategyParams;
import build.utils.ArqLogger;
import build.utils.StandardTimes;
import enums.PORTFOLIO_TYPE;
import enums.EXCHANGE;
import enums.STRATEGY_STATUS;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Alert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AroonadxSummary extends StrategyParams {

    private static final Logger m_logger = ArqLogger.getInstance().getLogger();
    private final SimpleStringProperty symbol;
    private final SimpleStringProperty createdTime;
    private final SimpleIntegerProperty emaPeriod1;
    private final SimpleIntegerProperty emaPeriod2;
    private final SimpleIntegerProperty emaPeriod3;
    private final SimpleIntegerProperty adxPeriod;
    private final SimpleIntegerProperty aroonPeriod;
    private final SimpleIntegerProperty interval;
    private final SimpleStringProperty indicator1;
    private final SimpleStringProperty indicator2;
    private final SimpleStringProperty indicator3;
    private final SimpleStringProperty indicator4;
    private final SimpleStringProperty indicator5;
    private final SimpleStringProperty name;
    private final SimpleStringProperty clientName;
    private final SimpleIntegerProperty margin;
    private final SimpleStringProperty portfolio;


    public int getMargin() {
        return margin.get();
    }

    public SimpleIntegerProperty marginProperty() {
        return margin;
    }

    public void setMargin(int margin) {
        this.margin.set(margin);
    }

    public String getClientName() {
        return clientName.get();
    }

    public SimpleStringProperty clientNameProperty() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName.set(clientName);
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime.set(createdTime);
    }


    public String getIndicator1() {
        return indicator1.get();
    }

    public SimpleStringProperty indicator1Property() {
        return indicator1;
    }

    public void setIndicator1(String indicator1) {
        this.indicator1.set(indicator1);
    }

    public String getIndicator2() {
        return indicator2.get();
    }

    public SimpleStringProperty indicator2Property() {
        return indicator2;
    }

    public void setIndicator2(String indicator2) {
        this.indicator2.set(indicator2);
    }

    public String getIndicator3() {
        return indicator3.get();
    }

    public SimpleStringProperty indicator3Property() {
        return indicator3;
    }

    public void setIndicator3(String indicator3) {
        this.indicator3.set(indicator3);
    }

    public String getIndicator4() {
        return indicator4.get();
    }

    public SimpleStringProperty indicator4Property() {
        return indicator4;
    }

    public void setIndicator4(String indicator4) {
        this.indicator4.set(indicator4);
    }

    public String getIndicator5() {
        return indicator5.get();
    }

    public SimpleStringProperty indicator5Property() {
        return indicator5;
    }

    public void setIndicator5(String indicator5) {
        this.indicator5.set(indicator5);
    }
    public double getSymbolLtp() {
        return symbolLtp.get();
    }

    public SimpleDoubleProperty symbolLtpProperty() {
        return symbolLtp;
    }

    public void setSymbolLtp(double symbolLtp) {
        this.symbolLtp.set(symbolLtp);
    }

    private final SimpleDoubleProperty symbolLtp;
    public String getExchange() {
        return exchange.get();
    }

    public SimpleStringProperty exchangeProperty() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange.set(exchange);
    }

    private final SimpleStringProperty exchange;

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public int getInterval() {
        return interval.get();
    }

    public SimpleIntegerProperty intervalProperty() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval.set(interval);
    }

    public int getEmaPeriod1() {
        return emaPeriod1.get();
    }

    public SimpleIntegerProperty emaPeriod1Property() {
        return emaPeriod1;
    }

    public void setEmaPeriod1(int emaPeriod1) {
        this.emaPeriod1.set(emaPeriod1);
    }

    public  int getEmaPeriod2() {
        return emaPeriod2.get();
    }

    public SimpleIntegerProperty emaPeriod2Property() {
        return emaPeriod2;
    }

    public void setEmaPeriod2(int emaPeriod2) {
        this.emaPeriod2.set(emaPeriod2);
    }

    public int getEmaPeriod3() {
        return emaPeriod3.get();
    }

    public SimpleIntegerProperty emaPeriod3Property() {
        return emaPeriod3;
    }

    public void setEmaPeriod3(int emaPeriod3) {
        this.emaPeriod3.set(emaPeriod3);
    }

    public String getSymbol() {
        return symbol.get();
    }

    public SimpleStringProperty symbolProperty() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol.set(symbol);
    }

    public int getAdxPeriod() {
        return adxPeriod.get();
    }

    public SimpleIntegerProperty adxPeriodProperty() {
        return adxPeriod;
    }

    public void setAdxPeriod(int adxPeriod) {
        this.adxPeriod.set(adxPeriod);
    }

    public int getAroonPeriod() {
        return aroonPeriod.get();
    }

    public SimpleIntegerProperty aroonPeriodProperty() {
        return aroonPeriod;
    }

    public void setAroonPeriod(int aroonPeriod) {
        this.aroonPeriod.set(aroonPeriod);
    }

    public AroonadxSummary()
    {
        symbol = new SimpleStringProperty();
        createdTime=new SimpleStringProperty(
                LocalTime.now()
                        .format(DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM)));
        emaPeriod1=new SimpleIntegerProperty();
        emaPeriod2=new SimpleIntegerProperty();
        emaPeriod3=new SimpleIntegerProperty();
        adxPeriod=new SimpleIntegerProperty();
        aroonPeriod=new SimpleIntegerProperty();
        interval=new SimpleIntegerProperty();
        name = new SimpleStringProperty();
        exchange = new SimpleStringProperty();
        symbolLtp=new SimpleDoubleProperty();
        indicator1=new SimpleStringProperty();
        indicator2=new SimpleStringProperty();
        indicator3=new SimpleStringProperty();
        indicator4=new SimpleStringProperty();
        indicator5=new SimpleStringProperty();
        clientName=new SimpleStringProperty();
        margin=new SimpleIntegerProperty();
        portfolio=new SimpleStringProperty();

    }

    @Override
    public EXCHANGE getDestination() {
        return EXCHANGE.NSE;
    }

    public String getCreatedTime() {
        return createdTime.get();
    }

    public SimpleStringProperty createdTimeProperty() {
        return createdTime;
    }

    @Override
    public boolean isSubscribed(String s) {
        return false;
    }

    @Override
    public boolean loadParams(String[] strings)
    {
        int count = 0;
        setPortfolio(strings[count++]);
        //setStatus(STRATEGY_STATUS.valueOf(strings[count++]));
        //setStatus(STRATEGY_STATUS.INITIALIZED);
        count++;
        setSymbol(strings[count++]);
        setSymbolLtp(Double.parseDouble(strings[count++]));
        setStartTime(strings[count++]);
        setEndTime(strings[count++]);
        setCreatedTime(strings[count++]);
        setEmaPeriod1(Integer.parseInt(strings[count++]));
        setEmaPeriod2(Integer.parseInt(strings[count++]));
        setEmaPeriod3(Integer.parseInt(strings[count++]));
        setAdxPeriod(Integer.parseInt(strings[count++]));
        setAroonPeriod(Integer.parseInt(strings[count++]));
        setInterval(Integer.parseInt(strings[count++]));
        setMargin(Integer.parseInt(strings[count++]));
        setClientName(strings[count++]);
        setStrategy(strings[count++]);
        setBroker(strings[count]);


        if(getBroker().equals("TEST"))
            setPortfolioType(PORTFOLIO_TYPE.PAPER_TRADING);
        else
            setPortfolioType(PORTFOLIO_TYPE.LIVE);

        return true;
    }

    @Override
    public String[] saveParams()
    {
        m_logger.log(Level.INFO, "AroonadxSummary: saveParams");
        String[] arr = new String[17];
        arr[0] = getPortfolio();
        arr[1] = "" + getStatus();
        arr[2]=  getSymbol();
        arr[3] =  ""+ getSymbolLtp();
        arr[4] = getStartTimeStr();
        arr[5] = getEndTimeStr();
        arr[6] =  "" + getCreatedTime();
        arr[7] = ""+getEmaPeriod1();
        arr[8] = ""+ getEmaPeriod2();
        arr[9] = ""+ getEmaPeriod3();
        arr[10] = ""+ getAdxPeriod();
        arr[11] = ""+ getAroonPeriod();
        arr[12] = ""+ getInterval();
        arr[13] = ""+ getMargin();
        arr[14] = getClientName();
        arr[15]=getStrategy();
        arr[16]=getBroker();
        return arr;
    }
    @Override
    public boolean importParams(String[] strings) {
        int count = 0;
        m_logger.log(Level.INFO,strings[count]);
        setPortfolio(strings[count++]);
        m_logger.log(Level.INFO,strings[count]);
        setStrategy(strings[count++]);
        m_logger.log(Level.INFO,strings[count]);
        setSymbol(strings[count++]);
        m_logger.log(Level.INFO,strings[count]);
        setStartTime(strings[count++]);
        m_logger.log(Level.INFO,strings[count]);
        setEndTime(strings[count++]);
        m_logger.log(Level.INFO,strings[count]);
        setCreatedTime(strings[count++]);
        m_logger.log(Level.INFO,strings[count]);
        setEmaPeriod1(Integer.parseInt(strings[count++]));
        m_logger.log(Level.INFO,strings[count]);
        setEmaPeriod2(Integer.parseInt(strings[count++]));
        m_logger.log(Level.INFO,strings[count]);
        setEmaPeriod3(Integer.parseInt(strings[count++]));
        m_logger.log(Level.INFO,strings[count]);
        setAdxPeriod(Integer.parseInt(strings[count++]));
        m_logger.log(Level.INFO,strings[count]);
        setAroonPeriod(Integer.parseInt(strings[count++]));
        m_logger.log(Level.INFO,strings[count]);
        setInterval(Integer.parseInt(strings[count++]));
        m_logger.log(Level.INFO,strings[count]);
        setMargin(Integer.parseInt(strings[count++]));
        m_logger.log(Level.INFO,strings[count]);
        setClientName(strings[count++]);
        m_logger.log(Level.INFO,strings[count]);
        setBroker(strings[count]);

        return true;
}
}
